import datetime
import json

import mysql.connector
from mysql.connector import errorcode

from utils import settings


def connection():
    try:
        # README
        # -> deploying with 'settings.DB_CONFIG'
        # -> test locally with 'settings.DB_CONFIG_LOCAL'
        cnx = mysql.connector.connect(**settings.DB_CONFIG)
        return cnx
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
            return errorcode
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
            return errorcode
        else:
            print(err)
            return err

def query(id):
    con = connection()
    cur = con.cursor()
    query = 'SELECT permission, payload, functions FROM injections WHERE id_injection=?'
    param = [id]
    cur.execute(query, param)
    data = cur.fetchall()
    con.close()
    return data


def operation_query(filename, path, package, type, input):
    now = datetime.now()
    date = now.strftime("%d_%m_%Y_%H_%M_%S")
    con = connection()
    cur = con.cursor()
    query = "INSERT INTO log (apk_name,apk_path,apk_package,operation_type,selected_input) VALUES(?,?,?,?,?)"
    params = [filename, path, package, type, input]
    cur.execute(query, params)
    con.commit()
    con.close()


def get_last_injection():
    query = "SELECT * FROM log WHERE operation_type= 'WRITE' ORDER BY operation_id DESC LIMIT 1;"
    con = connection()
    cur = con.cursor()
    cur.execute(query)
    data = cur.fetchall()
    con.close()
    print(data)
    return data


def get_last_attack_info():
    sql = "SELECT message FROM attack_result ORDER BY id_injection DESC LIMIT 1;"
    con = connection()
    cur = con.cursor()
    cur.execute(sql)
    data = cur.fetchall()
    con.close()
    print(data)
    return data


def attack_log():
    sql = "SELECT date, message FROM attack_result ORDER BY id_injection"
    con = connection()
    cur = con.cursor()
    cur.execute(sql)
    row_headers = [x[0] for x in cur.description]  # this will extract row headers
    rv = cur.fetchall()
    json_data = []
    for result in rv:
        json_data.append(dict(zip(row_headers, result)))
    return json.dumps(json_data)


def query_second_injection(id_injection):
    con = connection()
    cur = con.cursor()
    sql = 'SELECT permission, payload, functions FROM second_injections WHERE id_injection=?'
    param = [id_injection]
    cur.execute(sql, param)
    data = cur.fetchall()
    con.close()
    return data