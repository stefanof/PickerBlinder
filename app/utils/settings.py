# Flask settings
FLASK_HOST = '0.0.0.0'
FLASK_PORT = 9000
FLASK_DEBUG = False  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# Database settings
DB_USER = 'root'
DB_PASSWORD = 'y6Ia7!RsuXxNPppO'

# DB settings remote
DB_HOST = 'db'
DB_PORT = '3306'

DB_CONFIG = {
    'user': DB_USER,
    'password': DB_PASSWORD,
    'host': DB_HOST,
    'port': DB_PORT,
}

# Db settings local
DB_HOST_LOCAL = 'localhost'
DB_PORT_LOCAL = '32000'

DB_CONFIG_LOCAL = {
    'user': DB_USER,
    'password': DB_PASSWORD,
    'host': DB_HOST_LOCAL,
    'port': DB_PORT_LOCAL,
}